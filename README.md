### Ressources QGIS du CEN Alsace

Dépôt du CEN Alsace pour les styles QGIS, icônes et d'autres ressources qui peuvent être partagées avec l'extension [QGIS Resource Sharing][1] :

> https://plugins.qgis.org/plugins/qgis_resource_sharing/

Si vous souhaitez ajouter cette collection, vous pouvez suivre les instructions (en anglais) ici :

> https://qgis-contribution.github.io/QGIS-ResourceSharing/usage/adding-repository.html

avec l'URL : `https://gitlab.com/cenalsace/cen-qgis-ressources.git`

Types de ressources :

- Checklists
- Expressions
- Images
- Modèles
- Outils processing
- Scripts R
- Styles
- SVG
- Symboles

  [1]: https://plugins.qgis.org/plugins/qgis_resource_sharing/
